extends Resource
class_name WindowViewResource

export var name : String
export var width : int
export var height : int
export var fullscreen : bool
export var default : bool