extends Node
class_name WindowScaler

signal view_changed(view)


var scale_data : WindowScaleResource
var view_index : int


func load(data : WindowScaleResource):
	scale_data = data
	_start()


func process(delta: float) -> void:

	if (!scale_data):
		return

	if (scale_data.use_hotkey):
		if (Input.is_action_just_pressed("next_view")):
			_next_view()

func _start():

	view_index = -1

	if (scale_data.use_default):
		for view in (scale_data.views):
			view_index += 1
			var v = view as WindowViewResource
			if (v.default):
				_change_view(v)
				return

func _change_view(view : WindowViewResource):

	if (view.fullscreen):
		OS.window_fullscreen = true
	else:
		OS.window_fullscreen = false

	OS.window_size = Vector2(view.width, view.height)

	emit_signal("view_changed", view)


func _next_view():

	print(view_index)

	view_index += 1

	if (view_index > scale_data.views.size() - 1):
		view_index = 0

	_change_view(scale_data.views[view_index])


func _ready():
	print("hello")

