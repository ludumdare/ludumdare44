extends Resource
class_name WindowScaleResource

export var use_hotkey : bool
export var use_default : bool
export (Array, Resource) var views

