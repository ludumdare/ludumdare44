extends Button

export var load_scene : String
export var load_delay : float



func _on_LoadButton_pressed() -> void:
	SceneManager.transition_to(load_scene, load_delay)
