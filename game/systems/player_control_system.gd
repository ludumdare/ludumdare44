extends "./system.gd"

func on_process(entities, delta):

	for entity in entities:

		var movable = entity.get_component("movable") as Movable
		var last_next_loc = movable.next_loc
		var moveto = entity.get_component("moveto") as MoveTo
		var anim

		if (Game.current):
			if (Game.power_left > 0):
				if Input.is_action_just_pressed("action"):
					Game.current.queue = true
					anim = "action"
					Game.play_zap2()

		if (!moveto.is_moving):

			if Input.is_action_pressed("right"):
#				if (moveto.is_moving):
#					movable.next_loc = movable.last_loc + Vector2(1,0)
#				else:
				movable.next_loc = movable.trgt_loc + Vector2(1,0)
				movable.start = true
				anim = "right"

			if Input.is_action_pressed("left"):
#				if (moveto.is_moving):
#					movable.next_loc = movable.last_loc + Vector2(-1,0)
#				else:
				movable.next_loc = movable.trgt_loc + Vector2(-1,0)
				movable.start = true
				anim = "left"

			if Input.is_action_pressed("up"):
#				if (moveto.is_moving):
#					movable.next_loc = movable.last_loc + Vector2(0,-1)
#				else:
				movable.next_loc = movable.trgt_loc + Vector2(0,-1)
				movable.start = true
				anim = "up"

			if Input.is_action_pressed("down"):
#				if (moveto.is_moving):
#					movable.next_loc = movable.last_loc + Vector2(0,1)
#				else:
				movable.next_loc = movable.trgt_loc + Vector2(0,1)
				movable.start = true
				anim = "down"

		if !Game.tiles.has(movable.next_loc):
			movable.next_loc = last_next_loc
			movable.start = false
			anim = "idle"

		if anim:
			var ap = entity.get_node("AnimationPlayer") as AnimationPlayer
			ap.current_animation = anim



#		if (movable.is_moving):
#			print(entity.position, movable.last_loc, movable.next_loc, movable.trgt_loc)

