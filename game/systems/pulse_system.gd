extends "./system.gd"

func on_process(entities, delta):

	for entity in entities:

		var moveto = entity.get_component("moveto") as MoveTo
		var pulse = entity.get_component("pulse") as Pulse

		if pulse.shake:
			pulse.shake_delay_count += 1
			if (pulse.shake_delay_count > pulse.shake_delay):
				pulse.shake_delay_count = 0
				moveto.trgt_loc = pulse.nodes[0].global_position + _get_random_offset(5)
				moveto.is_moving = true


		if pulse.init:
			Logger.debug("init")
			pulse.init = false
			entity.position = pulse.nodes[0].global_position + _get_random_offset(5)

		if (pulse.start):
			Logger.debug("start")
			pulse.start = false
			pulse.trgt_node_index += 1
#			pulse.current.powered = true
			moveto.trgt_loc = pulse.nodes[pulse.trgt_node_index].global_position
			moveto.is_moving = true
			Logger.debug(moveto.trgt_loc)
			pulse.active = true
			pulse.shake = false
			moveto.speed = pulse.current.pulse_speed

		if (!pulse.active):
			continue

		if (moveto.complete):

			moveto.complete = false
			pulse.trgt_node_index += 1

			if (pulse.trgt_node_index > pulse.nodes.size() - 1):
				pulse.active = false
				ECS.remove_entity(entity)
				pulse.current.pulse_count -= 1
				Game.power_left += 1
				Game.play_zap1()
			else:
				moveto.trgt_loc = pulse.nodes[pulse.trgt_node_index].global_position
				Logger.debug(moveto.trgt_loc)
				moveto.is_moving = true


func _get_random_offset(size : int):
	return Vector2(rand_range(-size, size), rand_range(-size, size))
