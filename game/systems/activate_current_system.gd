"""
	System: ActivateCurrentSystem
		Activates a Current when Powered

"""
extends "./system.gd"

func on_process(entities, delta):

	for entity in entities:

		var current = entity.get_component("current") as Current

		if current.pulse_count > 0:
			current.powered = true
			current.set_node_color(current.COLOR)
		else:
			current.powered = false
			current.set_node_color(Color.blue)


		if current.start:
			current.start = false
			current.active = true

		if current.stop:
			current.stop = false

		if current.queue:
			current.timer.start()
			current.queue = false
			current.pulse_count += 1
			current.pulse_queue += 1
			Game.power_left -= 1
			var pulse = Game.Pulse.instance()
			current.add_child(pulse)
			var p = pulse.get_component("pulse") as Pulse
			p.current = current
			p.nodes.append(current.get_node(current.current_start))
			for node in current.current_nodes:
				p.nodes.append(current.get_node(node))
			p.nodes.append(current.get_node(current.current_end))
			current.pulse_nodes.append(pulse)
			p.init = true
			p.shake = true

		if current.pulse:
			current.pulse = false
			if current.pulse_queue > 0:
				current.pulse_queue -= 1
				var pulse = current.pulse_nodes.pop_front().get_component("pulse") as Pulse
				pulse.start = true