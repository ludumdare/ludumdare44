extends "./system.gd"

func on_process(entities, delta):

	for entity in entities:

		var door = entity.get_component("door") as Door
		var sprite = entity.find_node("DoorSprite") as Sprite
		var cell = ((entity.global_position - Vector2(8,8)) / Game.GRID_SIZE).round()

		var last_power = door.is_powered
		door.is_powered = true

		if (door.currents):
			for c in door.currents:
				var cn = door.get_node(c).get_component("current") as Current
				if (!cn.powered):
					door.is_powered = false

		# if the same -- ignore
		if (last_power == door.is_powered):
			continue

		if (door.is_powered):
			sprite.visible = false
			Game.play_dooropen()
			if (!Game.tiles.has(cell)):
				Game.tiles.append(cell)

		if (!door.is_powered):
			sprite.visible = true
			Game.play_doorclose()
			if (Game.tiles.has(cell)):
				Game.tiles.remove(Game.tiles.find(cell))
