extends "./system.gd"

func on_process(entities, delta):

	for entity in entities:

		var movable = entity.get_component("movable") as Movable
		var moveto = entity.get_component("moveto") as MoveTo

		if (movable.start):
			movable.start = false
			Game.emit_signal("player_move_started", movable.trgt_loc)
			moveto.trgt_loc = movable.next_loc * Game.GRID_SIZE
			movable.trgt_loc = movable.next_loc
			moveto.is_moving = true

		if (moveto.complete):
			Game.emit_signal("player_move_ended", movable.trgt_loc)
			moveto.complete = true

#			movable.last_loc = movable.trgt_loc
#			if (movable.next_loc > Vector2()):
#				moveto.trgt_loc = movable.next_loc * GRID_SIZE
#				movable.trgt_loc = movable.next_loc
#				movable.next_loc = Vector2()
#
#		if (entity.position.round() == movable.trgt_loc * GRID_SIZE):
#
#			movable.last_loc = movable.trgt_loc.round()
#			movable.is_moving = false
#
#			if (movable.next_loc > Vector2()):
#				movable.trgt_loc = movable.next_loc
#				movable.next_loc = Vector2()
#				movable.is_moving = true
#		else:
#
#			var _dir = (movable.trgt_loc - movable.last_loc).normalized()
#			var _vel = _dir * movable.speed * movable.speed_factor
#			entity.position += _vel * delta
