"""
	Component: Current
		Contains Data for a Current Entity
"""

extends Node
class_name Current

export (NodePath) var current_start
export (NodePath) var current_end
export (Array, NodePath) var current_nodes
export (Color) var COLOR

export (int) var queue_speed = 10
export (int) var pulse_speed = 10

var active : bool
var pulse : bool
var queue : bool
var start : bool
var stop : bool
var powered : bool
var pulse_count : int
var pulse_queue : int
var pulse_nodes = []

var timer : Timer

func set_node_color(color : Color):
	_change_node_color(current_start, color)
	_change_node_color(current_end, color)
	for n in current_nodes:
		_change_node_color(n, color)


func _change_node_color(node : NodePath, color : Color):
	var sprite = get_node(node).find_node("Sprite")
	sprite.modulate = color


func _ready() -> void:

	# add current to the game
	Game.currents.append(self)

	# add current start node
	Game.current_starts.append(get_node(current_start))

	# create a new timer for pulse anim
	timer = Timer.new()
	add_child(timer)
	timer.connect("timeout", self, "_on_timeout")
	timer.wait_time = 1.5 + (20 - queue_speed) * 0.45
	timer.one_shot = false
	timer.start()
	set_node_color(Color.darkblue)



func _on_timeout():

	# add another pulse
	pulse = true

