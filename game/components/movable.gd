"""
	Component: Movable
		Makes Entity Move

"""
extends Node2D
class_name Movable

export var SPEED : int
export var SPEED_FACTOR : int

var speed : int
var speed_factor : int
var last_loc : Vector2
var next_loc : Vector2
var trgt_loc : Vector2
var start : bool
var stop : bool
var is_moving : bool


func _ready() -> void:

	if SPEED:			speed = SPEED
	if SPEED_FACTOR:	speed_factor = SPEED_FACTOR

	last_loc = Vector2(0, 0)
	trgt_loc = Vector2(0, 0)
	next_loc = Vector2(0, 0)
	is_moving = false

