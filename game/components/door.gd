extends Node
class_name Door

export var IS_EXIT : bool = false
export (Array, NodePath) var CURRENTS

var is_powered : bool = true
var is_exit : bool
var currents = []

var parent : Node2D

func _ready() -> void:
	Logger.trace("[door] _ready")

	if IS_EXIT:			is_exit = IS_EXIT
	if CURRENTS:		currents = CURRENTS

func _init() -> void:
	Logger.trace("[door] _init")
	Game.door = self
