"""
	Component: MoveTo
		Contains Data for Entities That Use the MoveTo System
"""
extends Node2D
class_name MoveTo

export var SPEED : int
export var SPEED_FACTOR : int

var trgt_loc : Vector2
var complete : bool
var is_moving: bool
var speed : int
var speed_factor : int


func _ready() -> void:

	if SPEED:			speed = SPEED
	if SPEED_FACTOR:	speed_factor = SPEED_FACTOR
