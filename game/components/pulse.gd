"""
	Component: Pulse
		Contains Data for Pulse Entity
"""
extends Node2D
class_name Pulse

var nodes = []
var trgt_node_index : int
var start = false
var current : Current
var init : bool
var shake : bool
var shake_delay : int = 10
var shake_delay_count : int
var active : bool