extends Node

export (bool) var TEST
export (PackedScene) var TEST_LEVEL

# the maps
var levels = [
	"res://game/levels/level1.tscn",
	"res://game/levels/level2.tscn",
	"res://game/levels/level3.tscn",
	"res://game/levels/level4.tscn",
	"res://game/levels/level5.tscn",
	"res://game/levels/level6.tscn",
]

# game variables
var life = 3
var level_map
var player
var level = 0


func _clear_level():
	Logger.trace("[main] _clear_level")

	Game.currents = []
	Game.current_starts = []
	Game.tiles = []
	Game.door = null
	Game.player = null
	Game.current = null

	# remove all entities
	for entity in ECS.entities:
		ECS.remove_entity(entity)

	ECS.rebuild()

	# remove current map
	if level_map:
		remove_child(level_map)

	# and rebuild
	ECS.rebuild()


func _on_game_state_changed(state):
	Logger.trace("[main] _on_game_state_changed")

	match state:

		Game.GAME_STATE_NONE:
			Game.change_state(Game.GAME_STATE_RESET)

		Game.GAME_STATE_RESET:
			_game_reset()
			Game.change_state(Game.GAME_STATE_LOAD)

		Game.GAME_STATE_LOAD:
			_game_load()
			Game.change_state(Game.GAME_STATE_READY)

		Game.GAME_STATE_READY:
			_game_ready()
			Game.change_state(Game.GAME_STATE_RUN)

		Game.GAME_STATE_RUN:
			_game_run()

		Game.GAME_STATE_STOP:
			_game_stop()

		Game.GAME_STATE_EXIT:
			_game_exit()

		Game.GAME_STATE_WIN:
			_game_win()


func _game_exit():
	Logger.trace("[main] _game_exit")

	# ignore if we are testing a level
	if !TEST:

		level += 1

		if level > levels.size():
			Game.change_state(Game.GAME_STATE_WIN)
			return

	# regardless, load the scene
	Game.change_state(Game.GAME_STATE_LOAD)


# load a level
func _game_load():
	Logger.trace("[main] _game_load")
	_clear_level()

	if TEST:
		level_map = TEST_LEVEL.instance()
	else:
		level_map = load(levels[level-1]).instance()

	add_child(level_map)


# get the level ready for playing
func _game_ready():
	Logger.trace("[main] _game_ready")

	# prepare level
	Game.power_left = Game.power_total

	# find player start
	var player_start = find_node("PlayerStart", true, false)

	if player_start:
		player = Game.Player.instance()
		add_child(player)
		player.position = player_start.position - Vector2(8, 8)
		player_start.queue_free()
		var movable = player.get_component("movable") as Movable
		movable.last_loc = (player.position / Game.GRID_SIZE).round()
		movable.trgt_loc = movable.last_loc
		movable.next_loc = movable.last_loc
		ECS.add_entity(player)

	# attach camera to player
	var camera = Camera2D.new()
	player.add_child(camera)
	camera.current = true

	# find open tiles

	var fm = find_node("Floor", true, false)
	for t in fm.get_used_cells():
		Game.tiles.append(t)

	var dm = find_node("Decorators", true, false)
	for t in dm.get_used_cells():
		if Game.tiles.has(t):
			Game.tiles.remove(Game.tiles.find(t))

	var wm = find_node("Walls", true, false)
	for t in wm.get_used_cells():
		if Game.tiles.has(t):
			Game.tiles.remove(Game.tiles.find(t))


# reset the level
func _game_reset():
	Logger.trace("[main] _game_reset")

	# reset game stuff
	life = 3
	level = 1
	Game.coins = 0
	Game.speed = 0


# run the game
func _game_run():
	Logger.trace("[main] _game_run")
	pass


func _game_stop():
	Logger.trace("[main] _game_stop")
	pass


func _game_win():
	Logger.trace("[main] _game_win")
	_clear_level()
	pass


func _on_player_move_ended(cell):
	Logger.trace("[main] _on_player_move_ended")
	Logger.debug(cell)

	var pos = (cell * Game.GRID_SIZE) + Vector2(8,8)

	# check if on current node
	for node in Game.current_starts:
		if node.global_position == pos:
			var c = node.get_parent().get_component("current")
			Game.current = c
			Logger.debug("- found current node on cell %s" % [cell])

	# check if on exit
	_check_exit()

	player.get_node("AnimationPlayer").current_animation = "idle"


func _on_player_move_started(cell):
	Logger.trace("[main] _on_player_move_started")
	Logger.debug(cell)
	Game.current = null


func _activate_current(cell):
	Logger.trace("[main] _on_activate_current")
	print(cell)
	var pos = (cell * Game.GRID_SIZE) + Vector2(8,8)
	for node in Game.current_starts:
		if node.global_position == pos:
			var c = node.get_parent().get_component("current")
			c.start = true


func _deactivate_current(cell):
	Logger.trace("[main] _on_deactivate_current")
	var pos = (cell * Game.GRID_SIZE) + Vector2(8,8)
	for node in Game.current_starts:
		if node.global_position == pos:
			var c = node.get_parent().get_component("current")
			c.stop = true


func _check_doors():

	if !Game.door:
		return

	var powered = true
	for c in Game.currents:
		if !c.powered:
			powered = false

	Game.door.is_powered = powered


func _check_exit():
	var pos = player.global_position + Vector2(8, 8)
	if (pos == Game.door.parent.global_position):
		Game.change_state(Game.GAME_STATE_EXIT)


func _process(delta: float) -> void:

	ECS.update("common")

	match Game.state:

		Game.GAME_STATE_RUN:
			ECS.update("run")


func _ready() -> void:
	Game.connect("player_move_started", self, "_on_player_move_started")
	Game.connect("player_move_ended", self, "_on_player_move_ended")
	Game.connect("game_state_changed", self, "_on_game_state_changed")
	Game.change_state(Game.GAME_STATE_NONE)
	Game.play_music1()

func _init() -> void:
	pass