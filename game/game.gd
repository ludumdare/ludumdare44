extends Node

const GRID_SIZE 		= 16

enum {
	GAME_STATE_NONE,
	GAME_STATE_RESET,
 	GAME_STATE_LOAD,
	GAME_STATE_READY,
 	GAME_STATE_RUN,
	GAME_STATE_EXIT,
	GAME_STATE_STOP,
	GAME_STATE_WIN,
	GAME_STATE_SCORES,
	GAME_STATE_PAUSE,
	GAME_STATE_OVER,
	GAME_STATE_TITLE,
	GAME_STATE_ABOUT,
}

export (Resource) var window_scaler_data
export (PackedScene) var TEST_MAP

const Player = preload("res://game/entities/player/player.tscn")
const Pulse = preload("res://game/entities/pulse.tscn")

const SoundZap1 = preload('res://game/assets/zap1.ogg')
const SoundZap2 = preload('res://game/assets/zap2.ogg')
const SoundDoorOpen = preload('res://game/assets/doorOpen_1.ogg')
const SoundDoorClose = preload('res://game/assets/doorClose_4.ogg')

const Music1 = preload("res://game/assets/12 - Timeworn Pagoda.ogg")

# when player life changes
signal player_life_changed()

# when the player moves
signal player_move_started(cell)
signal player_move_ended(cell)

# when the game state changes
signal game_state_changed(state)

var state
var scaler : WindowScaler
var player : Node2D
var tiles = []
var door : Door
var currents = []
var current_starts = []
var power_total : int = 3
var power_left : int = 4
var current : Current
var speed : int
var coins : int

var music_player

func change_state(state):
	self.state = state
	emit_signal("game_state_changed", self.state)


func _process(delta: float) -> void:
	scaler.process(delta)


func _ready() -> void:
	scaler = WindowScaler.new()
	scaler.load(window_scaler_data)
	music_player = AudioStreamPlayer.new()
	self.add_child(music_player)


func play_music1():
	music_player.stream = Music1
	music_player.play()

func play_zap1():
	var player = AudioStreamPlayer.new()
	self.add_child(player)
	player.stream = SoundZap1
	player.play()


func play_zap2():
	var player = AudioStreamPlayer.new()
	self.add_child(player)
	player.stream = SoundZap2
	player.play()

func play_dooropen():
	var player = AudioStreamPlayer.new()
	self.add_child(player)
	player.stream = SoundDoorOpen
	player.play()

func play_doorclose():
	var player = AudioStreamPlayer.new()
	self.add_child(player)
	player.stream = SoundDoorClose
	player.play()

