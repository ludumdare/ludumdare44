extends Node


var tm : TileMap
var player_start
var level

func _start(map : String):

	level = load(map).instance() as Node
	add_child(level)

	player_start = level.find_node("PlayerStart")

	tm = level.find_node("Floor")

	for t in tm.get_used_cells():
		Game.tiles.append(t)

	tm = level.find_node("Decorators")

	for t in tm.get_used_cells():
		if Game.tiles.has(t):
			Game.tiles.remove(Game.tiles.find(t))


	#print(tiles)

	if player_start:
		Game.player = Game.Player.instance()
		add_child(Game.player)
		Game.player.position = player_start.position - Vector2(8, 8)
		player_start.queue_free()
		var movable = Game.player.get_component("movable") as Movable
		movable.last_loc = (Game.player.position / 16).round()
		movable.trgt_loc = movable.last_loc
		movable.next_loc = movable.last_loc
		ECS.add_entity(Game.player)


func _process(delta: float) -> void:
	$Label.text = ""
	$Label.text += "power_total: %s\n" % [Game.power_total]
	$Label.text += "power_left: %s\n" % [Game.power_left]

func _ready() -> void:

	Logger.set_logger_level(Logger.LOG_LEVEL_TRACE)
	_start(Game.Map1)

func _on_Add_pressed() -> void:
	if Game.power_left > 0:
		if Game.current:
			Game.current.queue = true


func _on_Map1_pressed() -> void:
	pass # Replace with function body.


func _on_Map2_pressed() -> void:
	pass # Replace with function body.
