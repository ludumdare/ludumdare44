extends Node

onready var door = $Door.get_component("door") as Door

func _on_PowerUp_pressed() -> void:
	door.is_powered = true


func _on_PowerDown_pressed() -> void:
	door.is_powered = false


func _process(delta: float) -> void:
	ECS.update()