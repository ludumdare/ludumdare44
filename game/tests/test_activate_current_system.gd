extends Node

onready var current1 = $Entities/Current1.get_component("current") as Current
onready var current2 = $Entities/Current2.get_component("current") as Current

func _process(delta: float) -> void:
	ECS.update()
	$Label.text = ""
	$Label.text += "powered_1: %s\n" % [current1.powered]
	$Label.text += "pulse_count_1: %s\n" % [current1.pulse_count]
	$Label.text += "pulse_queue_1: %s\n\n" % [current1.pulse_queue]
	$Label.text += "powered_2: %s\n" % [current2.powered]
	$Label.text += "pulse_count_2: %s\n" % [current2.pulse_count]
	$Label.text += "pulse_queue_2: %s\n" % [current2.pulse_queue]



func _on_Add1_pressed() -> void:
	current1.queue = true

func _on_Add2_pressed() -> void:
	current2.queue = true
