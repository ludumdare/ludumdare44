extends Label

func _process(delta: float) -> void:
	text = ""
	text += "life: %s\n" % [Game.power_left]
	text += "coins: %s\n" % [Game.coins]
	text += "speed: %s\n" % [Game.speed]